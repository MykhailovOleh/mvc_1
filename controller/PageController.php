<?php

class PageController extends Controller 
{
    public function index($slug)
    {     
        if($slug)
        {
            $Model = new PageModel();
            $page = $Model->getPage($slug);      
            $this->render('./view/page.php', ['page' => $page]);
        }
        else 
        {
            $this->render('./view/404.php');
        }  
        
    }
    
    public function getPages(){
        $this->render('./view/pages.php');
    }
}