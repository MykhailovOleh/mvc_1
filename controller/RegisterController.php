<?php

class RegisterController extends Controller 
{
    public function index()
    {      
        $this->render('./view/register.php');
          
    }
    
    public function save()
    {            
        $data = json_decode(file_get_contents('php://input'));
        
        $res = $this->valid($data);
        
        if($res===TRUE)
        {
            $Model = new RegisterModel();
            $Model->addUser($data);
        }
        
        echo json_encode($res); die();
    }
    
    private function valid($data)
    {
        $regEmail = '/^[a-zA-Z0-9_\-\.]{2,}@[a-zA-Z0-9_\-\.]{1,}\.[a-zA-Z]{2,6}$/';
        $regPass = '/^[a-zA-Z0-9]{2,100}$/';
        $regName = '/^[a-zA-Zа-яА-Я]{2,100}$/';

        $errors = [];

        if (!isset($data->email) || !preg_match($regEmail, $data->email)){
            $errors ['email'] = 'Невалидный email';
        
        }
        
        if (!isset($data->pass) || !preg_match($regPass, $data->pass)){
            $errors ['pass'] = 'Невалидный пароль';
        
        }
        
        if (!isset($data->login) || !preg_match($regName, $data->login)){
            $errors ['name'] = 'Невалидное имя';
        
        }
        
        if ($errors){
            return $errors;
        }
        
        return true;
    }
    
}