<?php



class HeaderController extends Controller 
{
    public function index(){
        
        
        $Model = new HeaderModel();
        
        $user_login = '';
        
        $token = $_SESSION['user']['token'];
        $front_token = $_COOKIE['token'];
        
        
        
        if ($token == $front_token) 
        {
            $user_login = $_SESSION['user']['login'];
        }
        
        $pages = $Model->getLinkPage();
        
        include './view/header.php';
    }
}