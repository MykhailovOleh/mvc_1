<?php

 spl_autoload_register(function ($class_name) {
     
        $path = './controller/'.$class_name . '.php';
        
       
     
        if (file_exists('./controller/'.$class_name . '.php')) {
            include './controller/'.$class_name . '.php';
        }
        
        if (file_exists('./model/'.$class_name . '.php')) {
            include './model/'.$class_name . '.php';
        }
        
    });