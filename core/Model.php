<?php

include './core/DB.php';

abstract class Model 
{ 
    protected function getDB()
    {
        return DB::getInstance();
    }
}
