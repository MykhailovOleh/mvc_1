<?php

class DB {
    private $dbh;
    
    private static $instance = NULL;
    
    public $test;

    public function __construct() 
    {
        include './setting/db_params.php';
        try
        {
            $this->dbh = new PDO("mysql:host={$params['host']};dbname={$params['name']};charset=UTF8", $params['user'], $params['pass']);
            
        }
        catch (PDOException $ex)
        {
            echo $ex->getMessage();
        }
    }
    
    public function query($sql, $type = true)
    {
        $result = $this->dbh->query($sql);
        
        if($type)
        {
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }
        else 
        {
            return $result->fetchAll(PDO::FETCH_ARRAY);
        } 
    }
    
    public function singleQuery($sql, $type = true)
    {
        $result = $this->dbh->query($sql);
        
        if($type)
        {
            return $result->fetch(PDO::FETCH_ASSOC);
        }
        else 
        {
            return $result->fetch(PDO::FETCH_ARRAY);
        } 
    }
    
    public static function getInstance()
    {
        if(!self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
