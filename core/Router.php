<?php

class Router{
    /**
     * string
     */
    private $route;
    /**
     * 
     * CONSTRUCTOR
     */
    public function __construct() {    
     /**
     * подключаем массив с роутами
     */
        include './setting/routing.php'; // присваиваем массив с роутами в локальную переменную класса
        $this->route = $_GET['route']; // присваиваем строку запроса пользователя в локальную переменную класса
        $this->routes = $routes;
    }
    /**
     * route
     */
    public function route()
    {
        /**
         * если роут пустой подключаем контроллер дом.страницы
         */
        if($this->route === NULL){
            // include './controller/HomeController.php';
            $Home = new HomeController();
            $Home->index();
        }
        
        /**
         * Если есть путь - разбираем его по слешу
         */
        if ($this->route) {
            
            $dataController = $this->getPach();
            
            if(isset($this->routes[$dataController['name']]))
            {
                $this->showController($dataController);  
            } 
            else{
                $this->getError(404);
            }
        }
        
    }
    private function showController($data)
    {
        
        
        $route = $this->routes[$data['name']];
        //подключаем нужный контроллер
        
       # include $route['pach'];
        /**
         * создаем экземпляр класса контроллера (объект)
         */
        $Controller = new $route['controller']();
             // вызываем метод контроллера       
        $Action = $route['action'];
        
        //проверяем на наличие доп параметра в запросе
        if(isset($route['slug']) && isset($data['slug']))
        {
            $Controller->$Action($data['slug']);
        }
        elseif ($route['slug'] && !isset($data['slug']))
        {
            $Controller->$Action(NULL);
        }
        else {
            $Controller->$Action();            
        }
        
    }

    /**
         * Получить даные из запроса пользователя
         * @return array
         */
    private function getPach()
    {
            // разбираем роут по слешу
        $result = [];
        $patch_routes = explode('/',$this->route);
        
        $result['name'] = $patch_routes[0];//получаем название контроллера
            //проверяем, есть ли в массиве с роутами данный контроллер
        if(isset($patch_routes[1]))
        {
            $result['slug'] = $patch_routes[1];
        } 
        return $result;
    }      
   

     private function getError($er){  //если роутер не валидный, подключаем стр 404
        include './controller/ErrorsController.php';
        $Errors = new ErrorsController();
        $Errors->getError($er);
     }
}
