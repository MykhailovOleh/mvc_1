<?php

abstract class Controller
{ 
    public function render($path, $data = [])
    {
        $Header = new HeaderController();
        $Header->index();  
        
        include $path;
        
        $Footer = new FooterController();
        $Footer->index();
    }
    
}