<?php

class PageModel extends Model
{ 
    public function getPage($id)
    {
        $db = $this->getDB();
        return $db->singleQuery("SELECT * FROM page WHERE id = {$id}");
    }
}