<?php

$routes = [
    'home' => [
        'controller' => 'HomeController',
        'pach' => 'controller/HomeController.php',
        'action' => 'index',
        'reg' => '/^[a-zA-Z]{1,10}$/',
    ],
    
    'page' => [
        'controller' => 'PageController',
        'pach' => 'controller/PageController.php',
        'action' => 'index',
        'slug' => [
           'reg' => '/^[0-9]{1,10}$/',
        ],
        
    ],
    
    'pages' => [
        'controller' => 'PageController',
        'pach' => 'controller/PageController.php',
        'action' => 'getPages',
    ],
    
    'register' => [
        'controller' => 'RegisterController',
        'pach' => 'controller/RegisterController.php',
        'action' => 'index',
    ],
    
    'login' => [
        'controller' => 'LoginController',
        'pach' => 'controller/LoginController.php',
        'action' => 'index',
    ],
    
    'register-save' => [
        'controller' => 'RegisterController',
        'pach' => 'controller/RegisterController.php',
        'action' => 'save',
    ]
];