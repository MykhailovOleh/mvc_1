<div id="registration">
        <h1>Регистрация</h1>
        <h3>(страница на стадии разработки)</h3>
        <form>
            <div class="row">
                <label for="login">Login</label>
                <input type="text" id ="login">
                <span class ="hide" id="login_err">Неправильное имя:(</span>
            </div>
            <div class="row">
                <label for="password">Password</label>
                <input type="password" id ="password">
                <span class ="hide" id="password_err">Невалидный пароль:(</span>
            </div>
            <div class="row">
                <label for="email">E-Mail</label>
                <input type="email" id ="email">
                <span class ="hide" id="email_err">Невалидный e-mail:(</span>
            </div>
            <div class="row">
                <button id="send" type="button">Отправить</button>
            </div>
        </form>

</div>

<script>

function sendRegister(){
    var login = document.getElementById('login');
    var pass = document.getElementById('password');
    var email = document.getElementById('email');
    var err_login = document.getElementById('login_err');
    var err_pass = document.getElementById('password_err');
    var err_email = document.getElementById('email_err');
    
    var data = {
        login: login.value, 
        pass: pass.value, 
        email: email.value
    };
    /*Отправка методом  FETCH*/
    fetch ('/register-save', {
        method:'POST',
        body: JSON.stringify(data)
    })
    .then(function(response) {
        return response.json();
    })
    .then(function(data) {
        
        err_email.className = 'hide';
        err_pass.className = 'hide';
        err_login.className = 'hide';
        
        if(data['name']) {
            err_login.className ='';
        }
        
        if(data['email']) {
            err_email.className ='';
        }
        
        if(data['pass']) {
            err_pass.className ='';
        }
    })
    .catch( console.log );
}
    
    var send = document.getElementById('send');
    send.onclick = sendRegister;
</script>