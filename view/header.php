<html>
    <head>
        <link href="/view/css/style.css" rel="stylesheet"/>
        <script defer src="/view/js/main.js" type="text/javascript"></script>
        
    </head>
    <body>
        
        <header>
                      
            <nav>
                <ul>             
                    <li><a href="/">Домой</a></li>
                    <?php foreach ($pages as $page){ ?>
                        <li>
                            <a href="/page/<?= $page['id']?>"><?= $page['name'] ?></a>
                        </li>
                    <?php } ?>
                        <li><a href="/register">Регистрация</a></li>
                        <?php if (!$user_login) { ?>
                                <li><a href="/login">Вход</a></li>
                            
                        <?php } else {?>
                        
                        <li><a href="#"><?= $user_login ?></a></li>
                </ul>
                
                        <?php } ?>
            </nav>
        </header>
        <div class="content">